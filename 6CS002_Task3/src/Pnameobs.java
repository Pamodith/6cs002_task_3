import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Pnameobs extends Obsvr{
	
	   public Pnameobs(Sub subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }

	   @Override
	   public void displayFrame() {
			label = new JLabel(MultiLinugualStringTable.getMessage(0));
			welcomeFrame.add(label);
			text = new JTextField(16); 
		    welcomeFrame.add(text);
		    button = new JButton("Ok");
			welcomeFrame.add(button);
			button.addActionListener(this); 
			welcomeFrame.pack();
	   }

}




