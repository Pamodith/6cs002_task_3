import java.util.ArrayList;
import java.util.List;

public class Sub{
	
	private List<Obsvr> observers = new ArrayList<Obsvr>();

	   public void attach(Obsvr observer){
	      observers.add(observer);		
	   }

	   public void notifyAllObservers(){
	      for (Obsvr observer : observers) {
	         observer.displayFrame();
	      }
	   } 	
	
}
