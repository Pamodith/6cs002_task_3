public class Cont {
   private Strat strategy;

   public Cont(Strat strategy){
      this.strategy = strategy;
   }

   public void executeStrategy(){
      strategy.autoPlay();
   }
}