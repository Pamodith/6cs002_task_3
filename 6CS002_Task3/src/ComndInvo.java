
public class ComndInvo {
	
	private Comnd placeDomino;
	
	public ComndInvo(PDCommand PDCommand) {
		this.placeDomino = PDCommand;
	}
	
	public void doPlaceDomino() {
		placeDomino.execute();
	}
	
	public void undoPlaceDomino() {
		placeDomino.unexecute();
	}

}
