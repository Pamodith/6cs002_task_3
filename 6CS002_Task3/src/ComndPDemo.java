
public class ComndPDemo {

	public static void main(String[] args) {
		Aardvark ardvark = new Aardvark(1);
		
		PDCommand PDCommand = new PDCommand(ardvark);
		
		ComndInvo commandInvoker = new ComndInvo(PDCommand);
		commandInvoker.doPlaceDomino();
		
		System.out.println("\nUndo the domino placement (Y/N)?");
		String undo = IOLibrary.getString();
		if(undo.equalsIgnoreCase("Y")) {
			commandInvoker.undoPlaceDomino();
		}
	}

}
