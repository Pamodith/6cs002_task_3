public class MenuCont {

	private Menu model;
	private MenuV view;
	
	public MenuCont(Menu model, MenuV view) {
		this.model = model;
		this.view = view;
	}
	public void setPlayerName(String name) {
		model.setName(name);
	}
	public String getPlayerName() {
		return model.getName();
	}
	public void updateView() {
		view.displayGUIView();
	}
	
}
