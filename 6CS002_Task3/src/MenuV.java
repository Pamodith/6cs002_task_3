public class MenuV { 
	
	public void displayGUIView() {
		Sub subject = new Sub();

	      new WMObsrvr(subject);
	      new Pnameobs(subject);
	      new MainMenu_Ob(subject);
	      new DMbsvr(subject);
	      
	      subject.notifyAllObservers();
	}
	
} 
