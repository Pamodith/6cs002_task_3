
public class RunSPattern {

	public static void main(String[] args) {
	      Cont context = new Cont(new OAP_one());		
	      context.executeStrategy();

	      context = new Cont(new OAP_two());		
	      context.executeStrategy();

	      context = new Cont(new OAP_three());		
	      context.executeStrategy();
	   }
	
}
